#include <stdio.h>
#include <string>
#include <string.h>
#include <errno.h>
#include <wiringPi.h>
#include <wiringSerial.h>
#include "common/common.h"

int main()
{
  GPS mygps;
  settings mysettings;
  int fd ;
  int count=0 ;
  unsigned int nextTime ;
char buffer[500] = {0};

  if ((fd = serialOpen ("/dev/serial0", 9600)) < 0)
  {
    fprintf (stderr, "Unable to open serial device: %s\n", strerror (errno)) ;
    return 1 ;
  }

  if (wiringPiSetup () == -1)
  {
    fprintf (stdout, "Unable to start wiringPi: %s\n", strerror (errno)) ;
    return 1 ;
  }
  socket_t mysocket;
  mysocket.setup_client(mysettings.get_gps_socket().c_str());
  while(1)
  {
    
    while (serialDataAvail (fd))
    {
        char byte = serialGetchar(fd);
        if(byte == '\n')
        {
            buffer[count] = byte;
            if(buffer[0] == '$' && buffer[3] == 'R')
            {
              printf("%s",buffer);
              mygps.parseNEMAstring(buffer);
              
                if(mysocket.data_avail())
                {
                  char * garbage = (char*) mysocket.receive_msg(500);
                  delete garbage;
		              printf("sending data to controller\n");
                  char* mymessage = mygps.createstring();
                  
                  //mysocket.send_msg((void*) " , ,", 4);
                  mysocket.send_msg((void*) mymessage, strlen(mymessage));
                  delete mymessage;
	              }
	              /* FD_ISSET(0, &rfds) will be true. */
              //fflush(stdout);
              //printf("latitude is %f\n" , mygps.Location.Latitude);
              //printf("longitude is %f\n" , mygps.Location.Longitude);
            }
            count = 0;
            memset(buffer,0,500);
        }
        else
        {
          buffer[count] = byte;
          count++;
        }
    }
}

  printf ("\n") ;
  return 0 ;
}
