include common/common.mk

SOURCE_FILES += main.cpp

LIBRARIES += -lwiringPi
WARNINGS = -Wall
DEBUG = -ggdb
CC = g++
all: main.cpp
	$(CC) $(WARNINGS) $(DEBUG) $(SOURCE_FILES) $(LIBRARIES) -o GPS
clean:
	rm ./GPS
